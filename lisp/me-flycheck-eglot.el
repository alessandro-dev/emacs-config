;;; me-flycheck-eglot --- flycheck + eglot integration  -*- lexical-binding: t; -*-
;;
;;; Commentary:
;;
;; This is a hack to make eglot use flycheck instead of flymake.
;; Taken from:
;; https://gist.github.com/zw963/b2f62dccd2b799111a208a2d47791334
;;
;;; Code:

(defvar-local me/flycheck-eglot--current-errors nil)

(defun me/flycheck-eglot-init (_ callback)
  "Initialize flycheck backend for eglot."
  (eglot-flymake-backend #'me/flycheck-eglot--on-diagnostics)
  (funcall callback 'finished me/flycheck-eglot--current-errors))

(defun me/flycheck-eglot--on-diagnostics (diags &rest _)
  (cl-labels
      ((flymake-diag->flycheck-err
        (diag)
        (with-current-buffer (flymake--diag-buffer diag)
          (flycheck-error-new-at-pos
           (flymake--diag-beg diag)
           (pcase (flymake--diag-type diag)
             ('eglot-note 'info)
             ('eglot-warning 'warning)
             ('eglot-error 'error)
             (_ (error "Unknown diagnostic type, %S" diag)))
           (flymake--diag-text diag)
           :end-pos (flymake--diag-end diag)
           :checker 'eglot
           :buffer (current-buffer)
           :filename (buffer-file-name)))))
    (setq me/flycheck-eglot--current-errors
          (mapcar #'flymake-diag->flycheck-err diags))
    (flycheck-buffer-deferred)))

(defun me/flycheck-eglot--eglot-available-p ()
  (bound-and-true-p eglot--managed-mode))

(flycheck-define-generic-checker 'eglot
  "Report `eglot' diagnostics using `flycheck'."
  :start #'me/flycheck-eglot-init
  :predicate #'me/flycheck-eglot--eglot-available-p
  :modes '(prog-mode text-mode))

(push 'eglot flycheck-checkers)

(defun me/flycheck-eglot-prefer-flycheck-h ()
  (when eglot--managed-mode
    (flymake-mode -1)
    (when-let ((current-checker (flycheck-get-checker-for-buffer)))
      (unless (equal current-checker 'eglot)
        (flycheck-add-next-checker 'eglot current-checker)))
    (flycheck-add-mode 'eglot major-mode)
    (flycheck-mode 1)
    (flycheck-buffer-deferred)))

(add-hook 'eglot-managed-mode-hook 'me/flycheck-eglot-prefer-flycheck-h)

(with-eval-after-load
    (when (and
           (not (fboundp 'flymake--diag-buffer))
           (fboundp 'flymake--diag-locus))
      (defalias 'flymake--diag-buffer 'flymake--diag-locus)))

(provide 'me-flycheck-eglot)
;;; me-flycheck-eglot.el ends here
