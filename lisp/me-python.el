;;; me-python --- my python config -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(require 'f)
(require 'eglot)
(require 'projectile)

;;;###autoload
(defun me/pyvenv-autoload ()
  "Autoload python virtual environment if it's found."
  (let* ((proot (or (projectile-project-root) default-directory))
         (pname (projectile-project-name))
         (venvs (pyvenv-virtualenv-list))
         (venv (f-join proot ".venv"))
         (workon (car (seq-filter (apply-partially #'equal pname) venvs)))
         (lsp (eglot-current-server))
         (venv-found t))
    (cond ((file-directory-p venv) (pyvenv-activate venv))
          (workon (pyvenv-workon workon))
          (t (setq venv-found nil)))
    (if (and venv-found lsp)
        (eglot-reconnect lsp))))

(provide 'me-python)
;;; me-python.el ends here
