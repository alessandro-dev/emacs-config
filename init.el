;;  -*- lexical-binding: t; -*-

(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))

;;; server
;; start the server if it's not running
(require 'server)
(unless (server-running-p) (server-start))

;;; package management

;;;; straight.el
;; straight.el provides a better way to pull packages not on elpa/melpa/gnu
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;;;; use-package
(straight-use-package 'use-package)
(setq use-package-hook-name-suffix nil)

;;; perfomance

;; garbage-collection seems to be the biggest perfomance detractor in Emacs
;; luckly there's a package that helps improve this by garbage-collecting in
;; a smarter way.

(use-package gcmh
  :straight t
  :init (gcmh-mode +1)
  :hook
  (minibuffer-setup-hook . gcmh-set-high-threshold)
  (minibuffer-exit-hook . gcmh-register-idle-gc)
  (focus-out-hook . garbage-collect))

;;; evil

(use-package evil
  :straight t
  :init (evil-mode +1)
  :custom
  (evil-want-C-u-scroll t)
  (evil-want-integration t)
  (evil-want-keybinding nil)
  (evil-undo-system 'undo-redo)
  (evil-want-fine-undo t)
  :config
  (setq evil-insert-state-cursor '(box "tomato")
        evil-visual-state-cursor '(box "SeaGreen")
        evil-normal-state-cursor '(box "DimGray")
        evil-replace-state-cursor '(hollow "red"))

  (evil-global-set-key 'insert (kbd "C-e") 'move-end-of-line)
  (evil-global-set-key 'insert (kbd "C-a") 'move-beginning-of-line)
  (evil-global-set-key 'insert (kbd "C-y") 'yank)

  (define-key evil-visual-state-map (kbd "J") (concat ":m '>+1" (kbd "RET") "gv=gv"))
  (define-key evil-visual-state-map (kbd "K") (concat ":m '<-2" (kbd "RET") "gv=gv"))
  (evil-global-set-key 'normal (kbd "C-`") 'window-toggle-side-windows))

(use-package evil-collection
  :straight t
  :after evil
  :config (evil-collection-init))

;;;; commentary
(use-package evil-commentary
  :straight t
  :after evil
  :config (evil-commentary-mode +1))

;;;; surround
(use-package evil-surround
  :straight t
  :after evil
  :config (global-evil-surround-mode +1))

;;;; traces
;; preview commands, like inccommand=nosplit for neovim
(use-package evil-traces
  :straight t
  :after evil
  :hook (evil-mode-hook . evil-traces-mode))

;;;; visualstar
;; allow to use * and # searches over a region
(use-package evil-visualstar
  :straight t
  :after evil
  :config (global-evil-visualstar-mode +1))

;;;; general
;; very useful wrapper around evil and emacs keybindings
(use-package general
  :straight t
  :after evil
  :config
  (general-evil-setup)

  (general-create-definer +leader
    :prefix "SPC"
    :global-prefix "M-SPC")

  (general-create-definer +local-leader
    :prefix "SPC m"
    :global-prefix "M-SPC m"))

;;;; avy
;; jump to anywhere on the screen

(use-package avy
  :straight t
  :after (general evil)
  :config
  (general-nmap "j" (general-key-dispatch 'evil-next-line
                      :timeout 0.1 "k" 'avy-goto-char-timer)))

;;; project

;; pull latest project.el
(use-package project
  :straight t
  :custom
  (project-compilation-buffer-name-function 'project-prefixed-buffer-name)
  :init
  (setq compilation-buffer-name-function #'project-prefixed-buffer-name)
  :config
  (defun project-find-file-other-project (project-root)
    "Find file in different PROJECT-ROOT."
    (interactive (list (project-prompt-project-dir)))
    (unless (file-directory-p project-root)
      (error "Project directory '%s' doesn't exist" project-root))
    (let ((default-directory project-root))
      (project-find-file)))

  (defun consult-project-compile--candidates ()
    "Return the list of candidates for compilation."
    (if (boundp 'compile-history)
        compile-history
      '()))

  (defun consult-project-compile--compile (cmd &rest _)
    "Run CMD on the project root."
    (let ((default-directory (project-root (project-current t)))
          (compilation-buffer-name-function
           (or project-compilation-buffer-name-function compilation-buffer-name-function)))
      (compile cmd)))

  (defun consult-project-compile ()
    "Select a compilation to run in the current project."
    (interactive)
    (consult--read (consult-project-compile--candidates)
                   :prompt "Compile: "
                   :require-match nil
                   :lookup #'consult-project-compile--compile)))

;; should this try to run only the test at point?
;; maybe allow passing the `universal-argument' to decide if
;; test file or only the closest test.
(defun me/ls-django-test-command ()
  "Run test on docker."
  (let ((test-cmd "bin/run.sh pytest"))
    (if (file-name-nondirectory (buffer-file-name))
        (format "%s %s" test-cmd (file-relative-name (buffer-file-name) (projectile-project-root)))
      test-cmd)))

(defun me/get-python-test-file (impl-file-path)
  "Return the corresponding test file directory for IMPL-FILE-PATH."
  (let* ((rel-path (f-relative impl-file-path (projectile-project-root)))
         (src-dir (car (f-split rel-path))))
    (cond ((f-exists-p (f-join impl-file-path "tests"))
           (f-join impl-file-path "tests"))
          ((f-exists-p (f-join (projectile-project-root) "test"))
           (projectile-complementary-dir impl-file-path src-dir "test"))
          ((f-exists-p (f-join (projectile-project-root) "tests"))
           (projectile-complementary-dir impl-file-path src-dir "tests"))
          (t (error "Could not locate a test file for %s!" impl-file-path)))))

;; better buffer name for projectile compilation
;; BUG: if the compile command is changed the `real-last-command' will be overriden by
;; `self-insert'.
(defvar me/projectie-task-name-map
  '((projectile-test-project . "test")
    (projectile-compile-project . "compile")
    (projectile-run-project . "run")))

(defun me/compilation-buffer-name (compilation-mode &optional task-name)
  (if (eq task-name nil)
      (project-prefixed-buffer-name compilation-mode)
    (project-prefixed-buffer-name (format "%s[%s]" compilation-mode task-name))))

(defun me/projectile-compilation-buffer-name (compilation-mode)
  (let* ((task-name (alist-get real-last-command me/projectie-task-name-map nil nil #'equal)))
    (me/compilation-buffer-name compilation-mode task-name)))

(advice-add 'projectile-compilation-buffer-name :override #'me/projectile-compilation-buffer-name)

(use-package projectile
  :straight t
  :custom
  (projectile-mode-line-function #'me/projectile-modeline-function)
  (projectile-per-project-compilation-buffer t)
  :hook
  (after-init-hook . projectile-mode)
  :config
  (defun me/projectile-modeline-function ()
    (if (projectile-project-p)
        (format " Proj[%s]" (projectile-project-name))
      ""))

  ;; LS django project types, leveraging both our docker-compose wrapper (bin/run.sh)
  ;; and the makefile.
  (projectile-register-project-type 'ls-django '("manage.py")
                                    :project-file "manage.py"
                                    :compile "make setup"
                                    :run "make run"
                                    :test 'me/ls-django-test-command
                                    :test-prefix "test_"
                                    :test-dir #'me/get-python-test-file)
  (projectile-register-project-type 'js-yarn '("yarn.lock")
                                    :project-file "yarn.lock"
                                    :compile "yarn"
                                    :run "yarn start"
                                    :test "CI=true yarn test"
                                    :test-suffix ".test.js"))

;;;; rg
;; projectile requires rg.el to integrate with ripgrep
(straight-use-package 'rg)

;;; ui

;; disable popups
(customize-set-variable 'use-dialog-box 'nil)

;;;; font

(defvar +me/font-name "VictorMono Nerd Font")
(defvar +me/font-height 130)

(set-face-attribute 'default nil        :font +me/font-name :height +me/font-height)
(set-face-attribute 'fixed-pitch nil    :font +me/font-name :height +me/font-height)
(set-face-attribute 'Info-quoted nil    :font +me/font-name :height +me/font-height)
(set-face-attribute 'variable-pitch nil :font +me/font-name :height +me/font-height)

;;;; theme

(use-package modus-themes
  :straight t
  :custom
  (modus-themes-italic-constructs t)
  (modus-themes-bold-constructs t)
  (modus-themes-subtle-line-numbers t)
  (modus-themes-fringes 'intense)
  (modus-themes-region '(accented))
  (modus-themes-mode-line '(accented))
  (modus-themes-paren-match '(bold intense))
  (modus-themes-syntax '(faint alt-syntax yellow-comments green-strings))
  (modus-themes-vivendi-color-overrides '((bg-main . "#100b17")))
  (modus-themes-hl-line '(intense)))

(use-package solar :straight (:type built-in))

;; switch between light and dark themes automatically
(use-package circadian
  :straight t
  :after (modus-themes solar)
  :custom
  (calendar-latitude -27.593500)
  (calendar-longitude -48.558540)
  (circadian-themes '((:sunrise . modus-operandi)
                      (:sunset . modus-vivendi)))
  :hook (window-setup-hook . circadian-setup))

;;;; modes
(tool-bar-mode -1)
(scroll-bar-mode -1)
(menu-bar-mode -1)
(blink-cursor-mode -1)
(savehist-mode +1)
(save-place-mode +1)
(electric-pair-mode +1)
(winner-mode +1)
(global-hl-line-mode +1)
(global-auto-revert-mode +1)
(recentf-mode +1)

;;;; fix auto revert on ibuffer
(add-to-list 'global-auto-revert-ignore-modes 'Buffer-menu-mode)

;;;; helpful
(use-package helpful
  :straight t
  :bind
  ([remap describe-function] . helpful-callable)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . helpful-variable)
  ([remap describe-key] . helpful-key)
  ([remap describe-symbol] . helpful-symbol))

;;;; uniquify
;; better buffer naming
(use-package uniquify
  :straight (:type built-in)
  :custom
  (uniquify-buffer-name-style 'forward))

;;;; hl-todo
(use-package hl-todo
  :straight t
  :hook (prog-mode-hook . hl-todo-mode))

;;;; diminish
(use-package diminish
  :straight t
  :after whitespace
  :config
  (diminish 'eldoc-mode)
  (diminish 'evil-collection-unimpaired-mode)
  (diminish 'unimpaired)
  (diminish 'apheleia-mode)
  (diminish 'apheleia-global-mode)
  (diminish 'gcmh-mode)
  (diminish 'evil-traces-mode)
  (diminish 'evil-commentary-mode)
  (diminish 'whitespace-mode)
  (diminish 'Outl)
  (diminish 'outline-mode)
  (diminish 'outline-minor-mode)
  (diminish 'yas)
  (diminish 'yas-minor-mode)
  (diminish 'yas-global-mode))

;;;; line numbers

(defun me/display-line-numbers-h ()
  (setq-local display-line-numbers 'relative
              display-line-numbers-type 'relative)
  (display-line-numbers-mode 1))

(defun me/hide-line-numbers-h ()
  (setq-local display-line-numbers nil
              display-line-numbers-type nil)
  (display-line-numbers-mode 0))

(defun me/toggle-line-numbers ()
  (if display-line-numbers-mode
      (me/hide-line-numbers-h)
    (me/display-line-numbers-h)))

(dolist (mode '(helpful-mode magit-mode))
  (add-hook (intern (format "%s-hook" mode)) #'me/hide-line-numbers-h))

(dolist (mode '(prog-mode text-mode))
  (add-hook (intern (format "%s-hook" mode)) #'me/display-line-numbers-h))

(+leader :states 'normal "tl" 'me/toggle-line-numbers)

;;; custom-file

(customize-set-variable 'custom-file (expand-file-name "custom.el" user-emacs-directory))
(load custom-file t)

;;; safe local variables
;; define the local variables which are always safe to be overriden in
;; .dir-locals.el or in the file local variables.

(put 'python-pytest-executable 'safe-local-variable 'stringp)

;;; misc settings
;; I don't know were to put this

(customize-set-variable 'indent-tabs-mode nil)
(customize-set-variable 'vc-follow-symlinks t)
(customize-set-variable 'use-short-answers t)
(customize-set-variable 'require-final-newline t)
(customize-set-variable 'compilation-scroll-output 'first-error)
(customize-set-variable 'auto-save-file-name-transforms `((".*" ,(expand-file-name "auto-save-list/" user-emacs-directory) t)))
(customize-set-variable 'backup-directory-alist `(("." . ,(expand-file-name "backups/" user-emacs-directory))))
(customize-set-variable 'global-auto-revert-non-file-buffers t)

;;; make scripts executable on save

(add-hook 'after-save-hook #'executable-make-buffer-file-executable-if-script-p)

;;; fix keys

;;;; C-[
;; for some reason emacs has decided that C-[ means ESC, why?
;; maybe they thought it would be funny, anyway this is necessary to override
;; that, but then when you want to use it you need to map `[control-bracketleft]'
;; instead of "C-[".
;; See this for more info: https://emacs.stackexchange.com/questions/7832/how-to-bind-c-for-real

(define-key input-decode-map (kbd "C-[") [control-bracketleft])

;;;; C-i
;; emacs maps TAB and C-i to the same thing, see more info here:
;; https://emacs.stackexchange.com/questions/17509/how-to-distinguish-c-i-from-tab
(define-key input-decode-map "\C-i" [control-i])

;;; fix path
;; emacs is not aware of my messed up $PATH

(customize-set-variable 'exec-path
                        (delq nil
                              (delete-dups
                               (append (list (expand-file-name "~/.local/bin/")
                                             (expand-file-name "~/go/bin")
                                             (expand-file-name "~/.cargo/bin"))
                                       exec-path))))
;;; helper functions

(defun me/reload-dir-locals-for-current-buffer ()
  "Reload dir locals for the current buffer."
  (interactive)
  (let ((enable-local-variables :all))
    (hack-dir-local-variables-non-file-buffer)))

(defun me/find-in-emacs-dir (&optional arg)
  "Find file in the emacs directory.
If `universal-argument' then go straight to init.el"
  (interactive "P")
  (if arg (find-file (expand-file-name "init.el" user-emacs-directory))
    (ido-find-file-in-dir user-emacs-directory)))

(defun me/find-in-org-dir (&optional arg)
  "Find file in the `org-directory'."
  (interactive)
  (ido-find-file-in-dir org-directory))

(defun me/yank-file-path ()
  "Yank path to the current file"
  (interactive)
  (let* ((dir (or (and (projectile-project-p) (projectile-project-root)) "/"))
         (fname (or (and (equal major-mode 'dired-mode) default-directory) (buffer-file-name)))
         (fpath (file-relative-name fname dir)))
    (kill-new fpath)
    (message "Yanked %s" fpath)))

;;; global keybindings

(+leader :states 'normal
;;;; global
  "u" 'universal-argument
  ";" 'execute-extended-command
  "'" 'eval-expression

;;;; buffers
  "bb" '(:def consult-buffer :package consult)
  "bk" 'kill-this-buffer
  "bl" 'ibuffer-other-window
  "br" 'revert-buffer

;;;; files
  "f." 'me/find-in-emacs-dir
  "fd" 'dired
  "ff" 'find-file
  "fy" 'me/yank-file-path

;;;; help
  "h" help-map

;;;; toggles / themes
  "t," '(:def modus-themes-toggle :package modus-themes)
  "tt" '(:def consult-theme :package consult)

;;;; windows
  "w" '(:keymap evil-window-map :package evil)
  "wm" 'maximize-window
  "wu" 'winner-undo

;;;; projects
  "pp" '(:def project-switch-project :package project)
  "pf" '(:def project-find-file :package project)
  "pt" '(:def projectile-test-project :package projectile)
  "pS" '(:def projectile-run-project :package projectile)
  "ps" '(:def projectile-ripgrep :package projectile)
  "pF" '(:def project-find-file-other-project :package project)
  "pc" '(:def consult-project-compile :package project)
  "px" '(:def projectile-run-vterm :package projectile)
  "pb" '(:def project-switch-to-buffer :package project)

;;;; code
  "ca" '(:def lsp-execute-code-action :package lsp)
  "cr" '(:def lsp-workspace-restart :package lsp)
  "cf" '(:def apheleia-format-buffer :package apheleia)
  "cd" '(:def flycheck-list-errors :package flycheck)

;;;; org
  "oc" '(:def org-capture :package org-capture)
  "of" 'me/find-in-org-dir)

;;; completion

(use-package orderless
  :straight t
  :custom
  (completion-styles '(orderless flex basic))
  (completion-category-overrides '((file (styles . (partial-completion)))))
  (read-buffer-completion-ignore-case t)
  (completion-category-defaults nil))

(use-package vertico
  :straight t
  :init (vertico-mode +1)
  :custom
  (vertico-cycle t)
  (vertico-resize t)
  (read-extended-command-predicate #'command-completion-default-include-p)
  :bind (:map vertico-map
              ("C-j" . vertico-next)
              ("C-k" . vertico-previous)))

(use-package marginalia
  :straight t
  :init (marginalia-mode 1)
  :custom
  (marginalia-annotators '(marginalia-annotators-heavy marginalia-annotators-light nil)))

(use-package consult
  :straight t
  :commands (consult-line consult-outline consult--read consult-buffer consult-theme)
  :bind
  ([remap isearch-forward] . consult-line))

(use-package embark
  :straight t
  :bind
  ([remap describe-bindings] . embark-bindings)
  ("C-;" . embark-act)
  :init
  (setq prefix-help-command #'embark-prefix-help-command))

(use-package corfu
  :straight t
  :hook (after-init-hook . global-corfu-mode)
  :custom
  (corfu-cycle t)
  (corfu-auto t)
  (corfu-auto-delay 0.1)
  (corfu-auto-prefix 2)
  (corfu-preselect-first t)
  (corfu-quit-at-boundary 'separator)
  (corfu-separator ?\s)
  (corfu-quit-no-match 'separator)
  (corfu-doc-auto nil)
  (completion-cycle-threshold 3)
  (tab-always-indent 'complete)
  :bind
  (:map corfu-map
        ("M-SPC" . corfu-insert-separator)
        ("RET" . corfu-insert)
        ("C-j" . corfu-next)
        ("C-k" . corfu-previous)))

(use-package cape
  :straight t
  :after corfu
  :general
  (:states 'insert "C-SPC" 'completion-at-point)
  (:states 'insert :prefix "C-x"
           "C-f" 'cape-file
           "C-s" 'hippie-expand
           "C-t" 'complete-tag)
  :init
  (add-to-list 'completion-at-point-functions #'cape-file)
  (add-to-list 'completion-at-point-functions #'cape-dabbrev))

;;; snippets

(use-package yasnippet
  :straight t
  :diminish t
  :init (yas-global-mode +1)
  :config
  (evil-define-key 'insert yas-minor-mode-map [control-i] 'yas-insert-snippet))

(use-package yasnippet-snippets
  :straight t
  :after yasnippet
  :config
  (yas-reload-all))

;;; version control
;; using magit and forge to manage git projects.

(use-package magit
  :straight t
  :commands (magit-status magit-clone magit-get-current-branch magit-blame)
  :custom
  (magit-diff-refine-hunk t))

(use-package forge
  :straight t
  :after magit
  :commands (forge-create-pullreq forge-create-issue forge-pull))

(use-package magit-todos
  :straight t
  :after magit
  :commands (magit-todos-mode)
  :hook (magit-mode-hook . magit-todos-mode))

(use-package git-link
  :straight t
  :commands (git-link)
  :after magit)

(+leader :states 'normal
  "gg" '(:def magit-status :package magit)
  "gC" '(:def magit-clone :package magit)
  "gb" '(:def magit-blame :package magit))

(+leader :states '(normal visual)
  "gl" '(:def git-link :package git-link))

;;; lsp

;; lsp client, seems lighter than lsp-mode for now
;; eglot was failing on js projects without any helpful output.
;; (use-package eglot
;;   :straight t
;;   :hook
;;   (python-mode-hook . eglot-ensure)
;;   (js-mode-hook . eglot-ensure)
;;   (js-jsx-mode-hook . eglot-ensure)
;;   (typescript-mode-hook . eglot-ensure)
;;   :init
;;   (setq read-process-output-max (* 1024 1024))
;;   :config
;;   (defun me/eglot-setup-h ()
;;     "Eglot setup hook function."
;;     (general-define-key :states 'normal :keymaps 'local
;;                         "gr" 'eglot-rename
;;                         "gR" 'xref-find-references
;;                         "gd" 'xref-find-definitions
;;                         "K" 'eldoc))
;;   (add-hook 'eglot--managed-mode-hook 'me/eglot-setup-h))

;; this changes lsp-mode compilation and should improve perfomance, see:
;; https://emacs-lsp.github.io/lsp-mode/page/performance/#use-plists-for-deserialization
(setenv "LSP_USE_PLISTS" "true")

(use-package lsp-mode
  :straight t
  :commands (lsp lsp-deferred)
  :init
  (setq read-process-output-max (* 1024 1024))
  :custom
  (lsp-diagnostics-provider :none)
  (lsp-completion-provider :none)
  (lsp-file-watch-threshold 2000)
  :hook
  (lsp-mode-hook . me/lsp-corfu-lsp-setup-h)
  :config
  (customize-set-variable
   'lsp-file-watch-ignored-directories
   (append (list "[/\\\\]staticfiles\\'"
                 "[/\\\\]__pycache__\\'"
                 "[/\\\\]static\\'"
                 "[/\\\\]\\.pytest_cache\\'"
                 "[/\\\\]terraform\\'"
                 "[/\\\\]templates\\'")
           lsp-file-watch-ignored-directories))

  (defun me/lsp-corfu-lsp-setup-h ()
    (setq-local completion-styles '(orderless)
                completion-category-defaults nil)))

;;; vterm
;; better terminal

(use-package vterm
  :straight t
  :custom
  (vterm-kill-buffer-on-exit t))

;;; tree-sitter
;; provides better and faster syntax highlight

(use-package tree-sitter
  :straight t
  :init (global-tree-sitter-mode)
  :hook
  (tree-sitter-after-on-hook . tree-sitter-hl-mode))

(use-package tree-sitter-langs
  :straight t
  :after tree-sitter)

;;; code formatting
;; + `apheleia' provides a wrapper around code formatting tools, and makes it easy
;;   to customize and extend them.
;; `editorconfig' provides simple guidelines to formatting text.

(use-package apheleia
  :straight t
  :diminish
  :init (apheleia-global-mode)
  :config
  (setf (alist-get 'python-mode apheleia-mode-alist) '(isort black)))

(use-package editorconfig
  :straight t
  :diminish
  :hook (after-init-hook . editorconfig-mode))

(use-package whitespace
  :straight (:type built-in)
  :hook (prog-mode-hook . whitespace-mode)
  :custom
  (whitespace-style '(face tabs trailing tab-mark))
  (whitespace-display-mappings '((space-mark 32 [183] [46])
                                 (newline-mark 10 [182 10])
                                 (tab-mark 9 [187 9] [9655 9] [92 9])))
  :config
  (set-face-attribute 'whitespace-tab nil :inherit 'magit-diff-our :background "LightPink" :weight 'bold)
  (set-face-attribute 'whitespace-trailing nil
                      :background "tomato"
                      :foreground "#183bc8"
                      :weight 'normal))

;;; lookup
;; dumb-jump provides a great option to find definitions when the language server fails to.

(use-package dumb-jump
  :straight t
  :hook (xref-backend-functions . dumb-jump-xref-activate)
  :custom
  (xref-show-definitions-function #'xref-show-definitions-completing-read)
  (dumb-jump-prefer-searcher 'rg)
  (dumb-jump-selector 'popup)
  :general
  (general-define-key :states '(normal visual motion) :keymaps 'global
                      "C-]" 'dumb-jump-go
                      [control-bracketleft] 'dumb-jump-back))

;;; elisp

;;;; local keybindings
(+local-leader :states 'normal :keymaps 'emacs-lisp-mode-map
  "o" '(:def consult-outline :package consult))

;;; python

(defun me/py-setup ()
  "Setup python tools."
  (require 'lsp-pyright)
  (lsp))

(use-package python-pytest
  :straight t
  :after python
  :commands (python-pytest-popup)
  :hook (python-pytest-started-hook . evil-force-normal-state))

(use-package pyvenv
  :straight t
  :after python
  :commands (pyvenv-activate pyvenv-workon pyvenv-virtualenv-list)
  :config
  (defun me/set-vterm-venv-h ()
    "Set vterm virtualenv variables."
    (add-to-list 'vterm-environment (format "VIRTUAL_ENV=%s" pyvenv-virtual-env)))

  (defun me/unset-vterm-venv-h ()
    "Set vterm virtualenv variables."
    (setq vterm-environment (remove (format "VIRTUAL_ENV=%s" pyvenv-virtual-env)
                                    vterm-environment)))

  (add-hook 'pyvenv-post-activate-hooks #'me/set-vterm-venv-h)
  (add-hook 'pyvenv-pre-deactivate-hooks #'me/unset-vterm-venv-h))

(use-package lsp-pyright
  :straight t
  :hook (python-mode-hook . me/py-setup))

;;;; local keybindings
(+local-leader :states 'normal :keymaps 'python-mode-map
  "tp" '(:def python-pytest-popup :package python-pytest)
  "tt" '(:def python-pytest-function :package python-pytest)
  "ea" '(:def pyvenv-activate :package pyvenv)
  "ew" '(:def pyvenv-workon :package pyvenv))

;;; js+ts

(defun me/js-setup ()
  "Setup JS tools, aslo applies to TS files."
  (add-node-modules-path)
  ;; (flycheck-select-checker 'javascript-eslint)
  (lsp))

(use-package js
  :hook (js-mode-hook . me/js-setup)
  :custom (js-indent-level 2))

(use-package add-node-modules-path
  :straight t
  :commands (add-node-modules-path))

(use-package typescript-mode
  :straight t
  :hook (typescript-mode-hook . me/js-setup)
  :mode ("\\.tsx\\'" . typescript-tsx-tree-sitter-mode)
  :custom
  (typescript-indent-level 2)
  :config
  (define-derived-mode typescript-tsx-tree-sitter-mode typescript-mode "TypeScript TSX")
  (add-to-list 'tree-sitter-major-mode-language-alist '(typescript-tsx-tree-sitter-mode . tsx)))

;;; yaml

(use-package yaml-mode
  :straight t
  :mode "\\.ya?ml\\'")

;;; outline

(use-package outline
  :straight (:type built-in)
  :diminish Outl
  :after general
  :hook
  (outline-minor-mode-hook . me/outline-mode-fix-keys-h)
  (outline-mode-hook . me/outline-mode-fix-keys-h)
  :config
  ;; evil overrides these keys, so we need to set it when entering outline
  (defun me/outline-mode-fix-keys-h ()
    (general-define-key :states '(normal motion) :keymaps 'outline-mode-map
                        "zk" 'outline-previous-heading
                        "zj" 'outline-next-heading)))

;;; org-mode
;; my org mode setup
(use-package org
  :straight (:host github :repo "emacs-straight/org-mode")
  :custom
  (org-link-descriptive nil)
  (org-directory (expand-file-name "~/Sync/org/"))
  (org-default-notes-file (expand-file-name "notes.org" org-directory))
  (org-todo-keyword '((sequence "TODO(t)"
                                "PROJ(p)"
                                "LOOP(r)"
                                "STRT(s)"
                                "WAIT(w)"
                                "HOLD(h)"
                                "IDEA(i)"
                                "|"
                                "DONE(d)"
                                "KILL(k)"
                                ;; The "NOTE" keyword is to mark a header as a simple note, pure informational.
                                "NOTE(n)")))
  :config
  (defvar me/org-default-journal-file (expand-file-name "journal.org" org-directory)
    "My org journal file.")
  (defvar me/org-default-bookmarks-file (expand-file-name "bookmarks.org" org-directory)
    "My bookmarks file."))

;;;; org-capture
(use-package org-capture
  :after org
  :commands (org-capture)
  :custom
  (org-capture-templates '(("t" "Personal todo" entry (file+headline org-default-notes-file "Inbox") "* TODO %?\n%i\n%a" :prepend t)
                           ("n" "Personal notes" entry (file+headline org-default-notes-file "Notes") "* %u %?\n%i\n%a" :prepend t)
                           ("j" "Journal" entry (file+olp+datetree me/org-default-journal-file) "* %U %?\n%i\n%a" :prepend t)
                           ("@" "Protocol Bookmark" entry (file+headline me/org-default-bookmarks-file "To-Read") "* TODO %:annotation %U" :immediate-finish t :empty-lines-before 2))))

;;;; protocol

;; org-protocol allows us to send requests to emacs from anywhere, this can be used to capture a web page
;; and store it as an org task item. This however requires a setup, see more here:
;; https://orgmode.org/worg/org-contrib/org-protocol.html
;; this is the current bookmarklet I'm using: ./org-capture-bookmarklet.js

(use-package org-protocol :after org :demand t)

;;;; org-mermaid
;; generate charts with mermaid

(use-package ob-mermaid
  :straight t
  :after org
  :config
  (org-babel-do-load-languages 'org-babel-load-languages '((mermaid . t))))

;;;; denote
;; not specificaly related to org mode, but I'll use it with it.

(use-package denote
  ;; TODO: change this to use GNU Elpa when it's available there
  :straight (:host github :repo "protesilaos/denote")
  :after (org org-capture)
  :custom
  (denote-directory (expand-file-name "denote" org-directory))
  :config
  (add-to-list 'org-capture-templates
               '("d" "Denote" plain (file denote-last-path) #'denote-org-capture :no-save t :immediate-finish nil :kill-buffer t :jump-to-captured t)))

;;;; org-indent

(use-package org-indent
  :after org
  :hook (org-mode-hook . org-indent-mode)
  :commands (org-indent-mode))

;;; bookmarking
;; I'm using harpoon (based on thePrimagen's harpoon) to navigate among files
;; in a project. I' also trying out `bookmark-in-project' as it uses Emacs built-in
;; bookmark libray.

(use-package harpoon
  :straight t
  :commands (harpoon-quick-menu-hydra harpoon-add-file)
  :general
  (+leader :states 'normal
    "SPC" '(:def harpoon-quick-menu-hydra :package harpoon)
    "." '(:def harpoon-add-file :package harpoon))
  :config
  ;; override harpoon candidates to use homerow instead of numbers
  ;; and show full file path.
  (defun me/harpoon-hydra-candidates (method)
    (let ((line-number 0)
          (full-candidates (seq-take (delete "" (split-string (harpoon--get-file-text) "\n")) 5)))
      (mapcar (lambda (item)
                (setq line-number (+ 1 line-number))
                (list (format "%s" (nth line-number '(nil "h" "j" "k" "l" ";")))
                      (intern (format "%s%s" method line-number))
                      item
                      :column "Go To"))
              full-candidates)))
  (advice-add 'harpoon--hydra-candidates :override #'me/harpoon-hydra-candidates))

(use-package bookmark-in-project
  :straight t
  :commands (bookmark-in-project-jump
             bookmark-in-project-jump-next
             bookmark-in-project-jump-previous
             bookmark-in-project-delete-all))

;;; pulsar.el
;; This package adds some improvements on top of the built-in pulse functions.
;; I've set it to pulse after evil jumps and also to pulse the yanked region.

(use-package pulsar
  :straight t
  :hook
  (after-init-hook . pulsar-global-mode)
  (consult-after-jump-hook . pulsar-recenter-middle)
  (consult-after-jump-hook . pulsar-reveal-entry)
  :custom
  (pulsar-pulse-on-window-change t)
  (pulsar-face 'pulsar-magenta)
  (pulsar-highlight-face 'pulsar-yellow)
  (pulsar-pulse-functions '(evil-goto-line
                            evil-backward-paragraph
                            evil-forward-paragraph
                            evil-scroll-up
                            evil-scroll-down
                            recenter-top-bottom
                            move-to-window-line-top-bottom
                            reposition-window
                            forward-page
                            backward-page
                            scroll-up-command
                            scroll-down-command
                            org-next-visible-heading
                            org-previous-visible-heading
                            org-forward-heading-same-level
                            org-backward-heading-same-level
                            outline-backward-same-level
                            outline-forward-same-level
                            outline-next-visible-heading
                            outline-previous-visible-heading
                            outline-up-heading))
  :config
  (defun me/evil-yank-advice (orig-fn beg end &rest args)
    (pulsar--pulse nil 'pulsar-yellow beg end)
    (apply orig-fn beg end args))

  (advice-add 'evil-yank :around 'me/evil-yank-advice))

;;; ansi-colors
;; fix ansi color codes on compilation mode

(use-package ansi-color
  :hook ((shell-mode-hook compilation-mode-hook comint-mode-hook) . ansi-color-for-comint-mode-on)
  :hook (compilation-filter-hook . me/colorize-compilation-h)
  :config
  (defun me/colorize-compilation-h ()
    "Colorize from `compilation-filter-start' to `point'."
    (let ((inhibit-read-only t))
      (ansi-color-apply-on-region compilation-filter-start (point)))))

;;; window management

(customize-set-variable 'display-buffer-alist
                        ;; display the help/helpful buffer to the right and to the bottom
                        `((,(rx bol "*" (any "H" "h") "elp" (optional "ful") (zero-or-more any) "*" eol)
                           (display-buffer-in-side-window)
                           (window-width . 0.23)
                           (side . right)
                           (slot . 0)
                           (reusable-frames . visible))
                          ;; display the eldoc buffer to the right and to the top
                          (,(rx bol "*eldoc" space (zero-or-more any) "*" eol)
                           (display-buffer-in-side-window)
                           (window-width . 0.23)
                           (side . right)
                           (slot . -1)
                           (reusable-frames . visible))
                          ;; display compilation process in the righ-most bottom buffer
                          (,(rx (or (group bol "*compilation*" eol)
                                    (group bol "*pytest*" (optional "<" (zero-or-more any) ">" eol)) ; pytest buffer
                                    (group bol "*" (optional (zero-or-more any)) "-compilation*" eol) ; project compilation buffer
                                    (group bol "*" (optional (zero-or-more any)) "-compilation[" (zero-or-more any) "]*" eol)
                                    (group bol "*compilation*<" (zero-or-more any) ">" eol) ; projectile compilation buffer
                                    ))
                           (display-buffer-in-side-window)
                           (window-height . 0.25)
                           (side . bottom)
                           (slot . 1)
                           (reusable-frames . visible))
                          ;; embark action buffer
                          ("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                           nil (window-parameters (mode-line-format . none)))
                          ;; display the terminal on the left-most bottom window
                          (,(rx bol "*vterm" (optional space (zero-or-more any)) "*" (optional "<" digit ">") eol)
                           (display-buffer-in-side-window)
                           (window-height . 0.25)
                           (side . bottom)
                           (slot . -1))
                          ;; display diagnostics at the bottom, to the right of the terminal
                          ;; and to the left of compilation.
                          (,(rx (or (group bol "*" (any "F" "f") "lymake" space "diagnostics" space (zero-or-more any) "*" eol)
                                    (group bol "*" (any "F" "f") "lycheck" space "errors" (zero-or-more any) "*" (zero-or-more any) eol)))
                           (display-buffer-in-side-window)
                           (window-height . 0.25)
                           (side . bottom)
                           (slot . 0))))

;;; flycheck

(use-package flycheck
  :straight t
  :hook (prog-mode-hook . flycheck-mode)
  :custom
  (flycheck-disabled-checkers '(javascript-jshint json-jsonlint))
  :config
  (flycheck-add-mode 'javascript-eslint 'typescript-mode)
  (flycheck-add-mode 'javascript-eslint 'typescript-tsx-tree-sitter-mode))

;;; dired

(customize-set-variable 'dired-listing-switches "-lXGhFp --group-directories-first")

(defun me/get-target-directory ()
  "Get the target directory, try first with projectile, then `vc-mode' then `default-directory'."
  (cond
   ((and (fboundp 'projectile-project-p) (projectile-project-p)) (projectile-project-root))
   ((not (equal (vc-root-dir) nil)) (expand-file-name (vc-root-dir)))
   (t default-directory)))

(defun prot/window-dired-vc-root-left ()
  "Open Dired in a side window.  Based on prot config."
  (interactive)
  (let ((dir (dired-noselect (me/get-target-directory))))
    (display-buffer-in-side-window dir
                                   `((side . left)
                                     (slot . -1)
                                     (window-width . 0.12)
                                     (window-parameters . ((no-delete-other-windows . t)
                                                           (mode-line-format . (" " mode-line-buffer-identification))))))
    (with-current-buffer dir
      (dired-hide-details-mode)
      (rename-buffer "*Dired-Side*")
      (setq-local window-size-fixed 'width))))

(with-eval-after-load 'projectile
  (define-key projectile-command-map "d" #'prot/window-dired-vc-root-left))

;;; hydras

(straight-use-package 'hydra)

(defhydra me/hydra-text-scale (:timeout 4)
  "Scale text."
  ("j" text-scale-increase "in")
  ("k" text-scale-decrease "out")
  (";" (text-scale-set) "reset")
  ("f" nil "finished" :exit t))

(defhydra me/hydra-window-resize (:timeout 4 :hint nil)
  "
^Resize window
^^^^^^^^^^^^^^
      ^_k_^
      ↑
  _h_ ←   → _l_
      ↓
      ^_j_^
"
  ("k" (shrink-window 2))
  ("j" (enlarge-window 2))
  ("h" (shrink-window-horizontally 2))
  ("l" (enlarge-window-horizontally 2)))

(+leader :states 'normal
  "tf" 'me/hydra-text-scale/body
  "wr" 'me/hydra-window-resize/body)

;;; workspaces

;; TODO: setup persp-mode workspaces
;; BUG: somehow persp mode messes up and tries to start eglot
;; even on non code buffers (like magit) and stuff just breaks.
;; DIABLED FOR NOW
(use-package persp-mode
  :disabled t
  :straight t
  :custom
  (persp-autokill-buffer-on-remove 'kill-weak)
  (persp-nil-name "main")
  :hook (window-setup-hook . persp-mode)
  :general
  (+leader :states 'normal [tab] 'persp-key-map)
  :bind (:map persp-key-map
              ([tab] . persp-switch)))

;;; minibuffer

;; TODO: make this more resilient, it will break on a non file buffer or a buffer outside
;; of a projectile project
(defun me/insert-current-buffer-name ()
  "Insert the path to the relative buffer in the minibuffer.
If the buffer is in a projectile project then insert relative to the root."
  (interactive)
  (insert (file-relative-name (buffer-file-name (window-buffer (minibuffer-selected-window))) (projectile-project-root))))

(define-key minibuffer-local-map (kbd "C-c f") 'me/insert-current-buffer-name)

;;; elfeed

(use-package elfeed
  :straight t
  :commands (elfeed elfeed-update)
  :custom
  (elfeed-feeds '(("https://xkcd.com/rss.xml" comic)
                  ("https://www.commitstrip.com/en/feed/" comic)
                  ("https://youtube.com/feeds/videos.xml?channel_id=UCAiiOTio8Yu69c3XnR7nQBQ" video emacs)
                  ("https://www.youtube.com/feeds/videos.xml?channel_id=UC0uTPqBCFIpZxlz_Lv1tk_g" video emacs)
                  ("https://www.youtube.com/feeds/videos.xml?channel_id=UCBMMB7Yi0eyFuY95Qn2o0Yg" video emacs)
                  ("https://sachachua.com/blog/feed" blog emacs))))

;;; html

(use-package web-mode
  :straight t
  :mode ("\\.html?\\'" . web-mode)
  :custom
  (web-mode-markup-indent-offset 2)
  (web-mode-code-indent-offset 2)
  (web-mode-css-indent-offset 2))

;;; footer
;; Local Variables:
;; outline-regexp: ";;;\\(;* [^ \t\n]\\)"
;; eval: (outline-minor-mode 1)
;; eval: (outline-hide-sublevels 1)
;; End:
