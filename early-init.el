(setq gc-cons-threshold (* 50 1000 1000))

(customize-set-variable 'load-prefer-newer noninteractive)

(setq package-enable-at-startup nil
      native-comp-async-report-warnings-errors nil
      native-comp-deferred-compilation nil)
