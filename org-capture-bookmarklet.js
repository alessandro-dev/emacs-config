javascript:(function () {
    /** Check if we are currently on a youtube video. */
    function isYtVideo (url) {
        return url.host.includes("youtube.com") && url.pathname === "/watch";
    }

    /** Get current timestamp for youtube player. */
    function ytGetCurrentTime() {
        try {
            const player = document.getElementById("movie_player");
            if (player && player.getCurrentTime) return Math.floor(player.getCurrentTime());
            return 0;
        } catch (err) {
            console.log("Could not get video timestamp.", err);
            return 0;
        }
    }

    /** Build URL to capture.
     * Unless it's a youtube video, return the page URL, else return the URL to the video
     * at the current time.
     */
    function buildUrl() {
        const url = new URL(window.location.href);
        if (!isYtVideo(url)) return window.location.href;
        const time = ytGetCurrentTime();
        const queryParams = new URLSearchParams(url.search);
        queryParams.set("t", `${time}s`);
        url.search = queryParams.toString();
        return url.toString();
    }

    /** Capture the current page on org protocol. */
    function capture() {
        const template = "@";
        const url = buildUrl();
        const title = document.title || "Untitled Page";
        const params = new URLSearchParams({ template, url, title });
        window.location.href = `org-protocol://capture?${params}`;
    }

    capture();
})();
